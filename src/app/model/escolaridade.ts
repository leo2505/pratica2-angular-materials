import { EstadoCivil } from './estadocivil';
import { Generos } from './generos';

export interface Escolaridade {
    idescolaridade: number;
    descricaoescolaridade?: string;
}