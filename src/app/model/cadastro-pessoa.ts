import { EstadoCivil } from './estadocivil';
import { Generos } from './generos';

export interface CadastroPessoa{
    idpessoa: number;
    nomepessoa?: string;
    datanascimentopessoa?: Date;
    telefone?: number;
    emailpessoa?: String;
    contatopessoa?: String;
    idgenero?: Generos;
    cpfpessoa?: number;
    idestadocivil?: EstadoCivil;
    //idescolaridade?: Escola
}