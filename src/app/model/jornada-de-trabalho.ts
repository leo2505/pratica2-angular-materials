export interface JornadaDeTrabalho{
    idhorario: number
    descricaohorario: string
    horasmensal: number
    inimanha?: string
    fimmanha?: string
    initarde?: string
    fimtarde?: string
}