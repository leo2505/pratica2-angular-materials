import { EstadoCivil } from './estadocivil';
import { Generos } from './generos';

export interface CadastroCandidato {
    idpessoa: number;
    nomepessoa?: string;
    dataNascimento?: Date;
    telefone?: number;
    emailpessoa?: String;
    contatopessoa?: String;
    idgenero?: Generos;
    cpfpessoa?: number;
    idestadocivil?: EstadoCivil;
    idcidade?: number
    //idescolaridade?: Escola
}