export interface LancamentoAvulso {
    idlancamento: number
    referencia: string
    valor: number
    idfuncionario: number
    idevento: number
}