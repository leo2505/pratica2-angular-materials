export interface CadastroFuncionarios{
    idpessoa: number
    idescolaridade?: number
    idcargo?: number
    idsetor?: number
    endereco?: string
    telefone?: string
    numerodependentes?: number
    salario?: any
    idfuncionario?: number
    nomefuncionario?: string
    ctpsfuncionario?:string
    orgaorgfuncionario?:string
}