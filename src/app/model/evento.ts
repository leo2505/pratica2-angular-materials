export interface Evento {
    idevento: number
    incideir: number
    incideinss: number
    incidefgts: number
    descricaoevento: string
    tipoevento: string
    eventofixo: number
}