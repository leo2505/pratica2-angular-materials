import { Setores } from './setores';
import { Cargos } from './cargos';
import { TipoVaga } from './tipovaga';

export interface CadastroVagas {
    idvaga: number
    descricaovaga: string
    atribuicoesvaga: string
    idsetor: number
    idcargo: number
    idtipovaga: number
}