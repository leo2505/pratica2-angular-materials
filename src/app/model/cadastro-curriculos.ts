export interface CadastroCurriculos {
    idcurriculo: number
    experienciascurriculo?: string
    habilidadescurriculo?: string
    idpessoa?: number
    idvaga?: number 
}