export interface ListaInss {
    idinss: number
    valorinicial: number
    valorfinal: number
    aliquota: number
    validade: Date
}