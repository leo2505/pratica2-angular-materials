export interface Dependentes{
    idfuncionario: number
    iddependente: number
    tipodependente: number
    nomedependente: string
   // rgdependente: string
    cpfdependente: string
    datanascimentodependente: Date
    depir: any
}