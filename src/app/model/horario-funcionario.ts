export interface HorarioFuncionario {
    mes: number
    ano: number
    idhorariofuncionario: number
    idfuncionario: number
    horastrabalhadas: number
    tipohoraextra: number
    diasuteis: number
    diasnaouteis: number
}