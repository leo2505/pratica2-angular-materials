export interface ListaIrrf {
    idirrf: number
    valorinicial: number
    valorfinal: number
    aliquota: number
    validade: Date
    deducaominima: number
    deducaodependente: number
}