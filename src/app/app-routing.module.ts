import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RecrutamentoSelecaoComponent } from './paginas/recrutamento-selecao/recrutamento-selecao.component';
import { CadastroVagasComponent } from './paginas/recrutamento-selecao/cadastro-vagas/cadastro-vagas.component';
import { CadastroCandidatoComponent } from './paginas/recrutamento-selecao/cadastro-candidato/cadastro-candidato.component';
import { HomeComponent } from './paginas/home/home.component';
import { SelecaoCandidatoComponent } from './paginas/recrutamento-selecao/selecao-candidato/selecao-candidato.component';
import { CadastroCurriculosComponent } from './paginas/recrutamento-selecao/cadastro-curriculo/cadastro-curriculos.component';
import { CargosComponent } from './paginas/configuracoes/cargos/cargos.component';
import { SetorComponent } from './paginas/configuracoes/setor/setor.component';
import { DependentesComponent } from './paginas/ficha-funcional/dependentes/dependentes.component';
import { CadastroPessoasComponent } from './paginas/ficha-funcional/cadastro-pessoas/cadastro-pessoas.component';
import { CadastroFuncionariosComponent } from './paginas/ficha-funcional/cadastro-funcionarios/cadastro-funcionarios.component';
import { JornadaDeTrabalhoComponent } from './paginas/configuracoes/jornada-de-trabalho/jornada-de-trabalho.component';
import { HorarioFuncionarioComponent } from './paginas/folha-pagamento/horario-funcionario/horario-funcionario.component';
import { TabelaInssComponent } from './paginas/folha-pagamento/tabela-inss/tabela-inss.component';
import { TabelaIrrfComponent } from './paginas/folha-pagamento/tabela-irrf/tabela-irrf.component';
import { LancamentoAvulsoComponent } from './paginas/folha-pagamento/lancamento-avulso/lancamento-avulso.component';
import { EventosComponent } from './paginas/folha-pagamento/eventos/eventos.component';
import { RelatorioPagamentoComponent } from './paginas/folha-pagamento/relatorio-pagamento/relatorio-pagamento.component';
import { RelatorioFuncionarioComponent } from './paginas/ficha-funcional/relatorio-funcionario/relatorio-funcionario.component';
import { ListaPessoasComponent } from './paginas/ficha-funcional/lista-pessoas/lista-pessoas.component';



const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'recrutamento-selecao', component: RecrutamentoSelecaoComponent },
  { path: 'cadastro-evento-recrutamento', component: CadastroVagasComponent },
  { path: 'cadastro-candidato', component: CadastroCandidatoComponent },
  { path: 'selecao-candidato', component: SelecaoCandidatoComponent },
  { path: 'cadastro-curriculo', component: CadastroCurriculosComponent },
  { path: 'cargos', component: CargosComponent },
  { path: 'setores', component: SetorComponent },
  { path: 'dependentes', component: DependentesComponent },
  { path: 'funcionarios', component: CadastroFuncionariosComponent },
  { path: 'jornada-de-trabalho', component: JornadaDeTrabalhoComponent },
  { path: 'pessoas', component: CadastroPessoasComponent },
  { path: 'lancamento-de-horas', component: HorarioFuncionarioComponent },
  { path: 'tabela-inss', component: TabelaInssComponent },
  { path: 'tabela-irrf', component: TabelaIrrfComponent },
  { path: 'lancamento-avulso', component: LancamentoAvulsoComponent },
  { path: 'evento', component: EventosComponent },
  { path: 'relatorio-pagamento', component: RelatorioPagamentoComponent },
  { path: 'relatorio-funcionario', component: RelatorioFuncionarioComponent },
  { path: 'lista', component: ListaPessoasComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
