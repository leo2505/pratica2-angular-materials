import { Component, OnInit } from '@angular/core';
import { CadastroSetoresService } from 'src/app/servicos/cadastro-setores.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroSetores } from 'src/app/model/cadastro-setores';
import { Setores } from 'src/app/model/setores';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-setor',
  templateUrl: './setor.component.html',
  styleUrls: ['./setor.component.css']
})
export class SetorComponent implements OnInit {
  titulonomemodulo: string = "Configurações"
  titulonomepagina: string = "Cadastro de Setores"
  constructor(private _cadastroSetores: CadastroSetoresService) { }

  ngOnInit() {

    this.load()
    this._cadastroSetores.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idsetor: new FormControl(),
      descricaosetor: new FormControl(),
    }
    )

    // console.log(this.myForm)
  }
  myForm: FormGroup
  lista: CadastroSetores[]
  alerta: any[] = []
  public load() {
    this._cadastroSetores.listar().subscribe(
      (result) => {
        this.lista = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API, tente novamente') : ''
      }
    )
  }

  public select(setor: CadastroSetores): void {
    this.myForm.patchValue(setor);
 }

  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as CadastroSetores;

    this._cadastroSetores.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('Erro ao tentar cadastrar! Tente novamente')
      }, () => {
        // console.log('Cadastrado');
        alert('Cadastrado com sucesso!')

      }
    )

    this._cadastroSetores.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(setor?: Setores) {
    let retorno = confirm('Deseja Excluir?');
    this._cadastroSetores.delete(setor.idsetor).subscribe(
      (resultado) => {
        alert('Cadastrado com sucesso');
        this.load();
      }, () => {

      }
    );
  }
}
