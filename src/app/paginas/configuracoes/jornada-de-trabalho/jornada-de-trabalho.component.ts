import { Component, OnInit } from '@angular/core';
import { JornadaDeTrabalhoService } from 'src/app/servicos/jornada-de-trabalho.service';
import { FormGroup, FormControl } from '@angular/forms';
import { JornadaDeTrabalho } from 'src/app/model/jornada-de-trabalho';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';

@Component({
  selector: 'app-jornada-de-trabalho',
  templateUrl: './jornada-de-trabalho.component.html',
  styleUrls: ['./jornada-de-trabalho.component.css']
})
export class JornadaDeTrabalhoComponent implements OnInit {
  titulonomemodulo: string = "Configurações"
  titulonomepagina: string = "Jornada de Trabalho"
  constructor(private _jornadaDeTrabalho: JornadaDeTrabalhoService) { }

  ngOnInit() {

    this.load()
    this._jornadaDeTrabalho.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idhorario: new FormControl(),
      inimanha: new FormControl(),
      fimmanha: new FormControl(),
      initarde: new FormControl(),
      fimtarde: new FormControl(),
      descricaohorario: new FormControl(),
      horasmensal: new FormControl(),
    }
    )

    // console.log(this.myForm)
  }
  myForm: FormGroup
  lista: JornadaDeTrabalho[]

  public load() {
    this._jornadaDeTrabalho.listar().subscribe(
      (result) => {
        this.lista = result
      }
    )
  }
  public select(JornadaDeTrabalho: JornadaDeTrabalho): void {
    this.myForm.patchValue(JornadaDeTrabalho);
 }

  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as JornadaDeTrabalho;

    this._jornadaDeTrabalho.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        alert('Cadastrado com sucesso!')

      }
    )

    this._jornadaDeTrabalho.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(jornada?: JornadaDeTrabalho) {
    let retorno = confirm('Deseja Excluir?');
    if (retorno){
      this._jornadaDeTrabalho.delete(jornada.idhorario).subscribe(
        (resultado) => {
          alert('Deletado com sucesso!');
          this.load();
        }, () => {
  
        }
      );
    }
    
  }
}
