import { Component, OnInit, Inject } from '@angular/core';
import { CargosService } from 'src/app/servicos/cargos.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Cargos } from 'src/app/model/cargos';
import { MatDialog } from '@angular/material';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { SucessoComponent } from 'src/app/componentes/sucesso/sucesso.component';
import { HttpErrorResponse } from '@angular/common/http';


@Component({
   selector: 'app-cargos',
   templateUrl: './cargos.component.html',
   styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit {
   titulonomemodulo: string = "Configurações"
   titulonomepagina: string = "Cadastro de Cargos"
   constructor(private _cadastroCargos: CargosService, private dialog: MatDialog) { }

   ngOnInit() {

      this.load()
      this._cadastroCargos.listar().subscribe(
         (result) => { this.lista = result }
      )
      this.myForm = new FormGroup({
         idcargo: new FormControl(),
         descricaocargo: new FormControl(),
         cbocargo: new FormControl(),
      }
      )
      // console.log(this.myForm)
   }

   openDialog() {
      const dialogRef = this.dialog.open(ModalComponent);

      dialogRef.afterClosed().subscribe(result => {
         // console.log(`Dialog result: ${result}`);
      });
   }
   myForm: FormGroup
   lista: Cargos[]
   alerta: any[] = []
   public load() {
      this._cadastroCargos.listar().subscribe(
         (result) => {
            this.lista = result
            // console.log(result)
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API, tente novamente') : ''
         }
      )
   }

   public select(cargo: Cargos): void {
      this.myForm.patchValue(cargo);
   }

   public save(): void {
      const cli =
         Object.assign({}, this.myForm.value) as Cargos;

      this._cadastroCargos.save(cli).subscribe(
         (result) => {
            this.myForm.reset();
            this.load();
         },
         (erro) => {
            alert('erro: ' + erro.message);
         }, () => {
            alert('Cadastrado com sucesso!');
            // this.openDialog()
            // alert('Cadastrado com sucesso!')

         }
      )

      this._cadastroCargos.listar().subscribe(
         (result) => { this.lista = result }
      )
   }
   public delete(cargo?: Cargos) {
      let retorno = confirm('Deseja Excluir?');
      this._cadastroCargos.delete(cargo.idcargo).subscribe(
         (resultado) => {
            alert('Deletado com sucesso!');
            this.load();
         }, () => {

         }
      );
   }
}