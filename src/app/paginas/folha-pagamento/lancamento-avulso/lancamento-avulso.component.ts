import { Component, OnInit } from '@angular/core';
import { LancamentoAvulsoService } from 'src/app/servicos/lancamento-avulso.service';
import { MatDialog } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';
import { LancamentoAvulso } from 'src/app/model/lancamento-avulso';
import { HttpErrorResponse } from '@angular/common/http';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { Evento } from 'src/app/model/evento';
import { EventoService } from 'src/app/servicos/evento.service';

@Component({
  selector: 'app-lancamento-avulso',
  templateUrl: './lancamento-avulso.component.html',
  styleUrls: ['./lancamento-avulso.component.css']
})
export class LancamentoAvulsoComponent implements OnInit {

  titulonomemodulo: string = "Folha de Pagamento"
  titulonomepagina: string = "Lançamento Evento"

  myForm: FormGroup
  listaLanc: LancamentoAvulso[]
  listafuncionario: CadastroFuncionarios[]
  listaEvento: Evento[]
  alerta: any[] = []

  constructor(
    private _lancAvulso: LancamentoAvulsoService,
    private _funcionario: CadastroFuncionariosService,
    private _eventoService: EventoService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.load()
    this._lancAvulso.listar().subscribe(
      (result) => { this.listaLanc = result }
    )
    this._eventoService.listar().subscribe(
      (result) => { this.listaEvento = result }
    )
    this.myForm = new FormGroup({
      idlancamento: new FormControl(),
      idfuncionario: new FormControl(),
      valor: new FormControl(),
      // funcionario: new FormControl(),
      idevento: new FormControl(),
      referencia: new FormControl()
    }
    )

  }
  public load() {
    this._lancAvulso.listar().subscribe(
      (result) => {
        this.listaLanc = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Lançamento Avulso, tente novamente') : ''
      }
    )
    this._funcionario.listar().subscribe(
      (result) => {
        this.listafuncionario = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Funcionários, tente novamente') : ''
      }
    )
    this._eventoService.listar().subscribe(
      (result) => {
        this.listaEvento = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Eventos, tente novamente') : ''
      }
    )
  }
  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent)
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  public select(lancamentoAvulso: LancamentoAvulso): void {
    this.myForm.patchValue(lancamentoAvulso);
  }

  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as LancamentoAvulso;

    this._lancAvulso.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        alert('Cadastrado com sucesso!');
        // this.openDialog()
      }
    )

    this._lancAvulso.listar().subscribe(
      (result) => { this.listaLanc = result }
    )
    this._eventoService.listar().subscribe(
      (result) => { this.listaEvento = result }
    )

  }
  public delete(lancamento?: LancamentoAvulso) {
    let retorno = confirm('Deseja Excluir?');
    if (retorno) {
      this._lancAvulso.delete(lancamento.idlancamento).subscribe(
        (resultado) => {
          alert('Deletado com sucesso!');
          this.load();
        }
      );
    }
  }
}