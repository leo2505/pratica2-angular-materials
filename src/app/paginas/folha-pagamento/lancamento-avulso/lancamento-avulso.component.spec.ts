import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LancamentoAvulsoComponent } from './lancamento-avulso.component';

describe('LancamentoAvulsoComponent', () => {
  let component: LancamentoAvulsoComponent;
  let fixture: ComponentFixture<LancamentoAvulsoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LancamentoAvulsoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LancamentoAvulsoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
