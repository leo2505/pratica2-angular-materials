import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ListaInss } from 'src/app/model/lista-inss';
import { InssService } from 'src/app/servicos/inss.service';
import { MatDialog } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';

@Component({
  selector: 'app-tabela-inss',
  templateUrl: './tabela-inss.component.html',
  styleUrls: ['./tabela-inss.component.css']
})
export class TabelaInssComponent implements OnInit {
  titulonomemodulo: string = "Folha de Pagamento"
  titulonomepagina: string = "Tabela INSS"

  myForm: FormGroup
  listainss: ListaInss[]
  alerta: any[] = []

  constructor(
    private _listaInss: InssService,
    private dialog: MatDialog,
  ) { }

  ngOnInit() {
    this.load()
    this._listaInss.listar().subscribe(
      (result) => { this.listainss = result }
    )
    this.myForm = new FormGroup({
      idinss: new FormControl(),
      valorinicial: new FormControl(),
      valorfinal: new FormControl(),
      aliquota: new FormControl(),
      validade: new FormControl()
    }
    )
  }
  public load() {
    this._listaInss.listar().subscribe(
      (result) => {
        this.listainss = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Lançamento de INSS, tente novamente') : ''
      }
    )
  }
  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent)
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  public select(inss: ListaInss): void {
    this.myForm.patchValue(inss);
  }

  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as ListaInss;

    this._listaInss.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        alert('Cadastrado com sucesso!');
        // this.openDialog()
      }
    )

    this._listaInss.listar().subscribe(
      (result) => { this.listainss = result }
    )
  }
  public delete(inss?: ListaInss) {
    let retorno = confirm('Deseja Excluir?');
    if (retorno) {
      this._listaInss.delete(inss.idinss).subscribe(
        (resultado) => {
          alert('Deletado com sucesso!');
          this.load();
        }
      );
    }

  }

}
