import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';
import { HorarioFuncionarioService } from 'src/app/servicos/horario-funcionario.service';
import { HorarioFuncionario } from 'src/app/model/horario-funcionario';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { MatDialog } from '@angular/material';
export interface Mes {
  mes: number;
  nome: string;
}
@Component({
  selector: 'app-horario-funcionario',
  templateUrl: './horario-funcionario.component.html',
  styleUrls: ['./horario-funcionario.component.css']
})

export class HorarioFuncionarioComponent implements OnInit {
  titulonomemodulo: string = "Folha de Pagamento"
  titulonomepagina: string = "Lançamento de Horas"
  meses: Mes[] = [
    { mes: 1, nome: 'Janeiro' },
    { mes: 2, nome: 'Fevereiro' },
    { mes: 3, nome: 'Março' },
    { mes: 4, nome: 'Abril' },
    { mes: 5, nome: 'Maio' },
    { mes: 6, nome: 'Junho' },
    { mes: 7, nome: 'Julho' },
    { mes: 8, nome: 'Agosto' },
    { mes: 9, nome: 'Setembro' },
    { mes: 10, nome: 'Outubro' },
    { mes: 11, nome: 'Novembro' },
    { mes: 12, nome: 'Dezembro' },
  ];

  myForm: FormGroup
  lista: HorarioFuncionario[]
  listafuncionario: CadastroFuncionarios[]
  alerta: any[] = []


  constructor(
    private _lancamentHorarioFuncionario: HorarioFuncionarioService,
    private dialog: MatDialog,
    private _funcionario: CadastroFuncionariosService
  ) { }

  ngOnInit() {
    this.load()
    this._lancamentHorarioFuncionario.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idhorariofuncionario: new FormControl(),
      idfuncionario: new FormControl(),
      mes: new FormControl(),
      ano: new FormControl(),
      horastrabalhadas: new FormControl(),
      tipohoraextra: new FormControl(),
      diasuteis: new FormControl(),
      diasnaouteis: new FormControl(),
    }
    )
  }

  public load() {
    this._lancamentHorarioFuncionario.listar().subscribe(
      (result) => {
        this.lista = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Lançamento de Horas, tente novamente') : ''
      }
    )
    this._funcionario.listar().subscribe(
      (result) => {
        this.listafuncionario = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Funcionários, tente novamente') : ''
      }
    )
  }
  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent)
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }

  public select(HorarioFuncionario: HorarioFuncionario): void {
    this.myForm.patchValue(HorarioFuncionario);
 }


  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as HorarioFuncionario;

    this._lancamentHorarioFuncionario.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        alert('Cadastrado com sucesso!');
        // this.openDialog()
        // alert('Cadastrado com sucesso!')

      }
    )

    this._lancamentHorarioFuncionario.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(HorarioFuncionario: HorarioFuncionario) {
    let retorno = confirm('Deseja Excluir?');
      this._lancamentHorarioFuncionario.delete(HorarioFuncionario.idhorariofuncionario).subscribe(
       (resultado) => {
          alert('Deletado com sucesso!');
          this.load();
       }, () => {

       }
    );
 }

}
