import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorarioFuncionarioComponent } from './horario-funcionario.component';

describe('HorarioFuncionarioComponent', () => {
  let component: HorarioFuncionarioComponent;
  let fixture: ComponentFixture<HorarioFuncionarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorarioFuncionarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorarioFuncionarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
