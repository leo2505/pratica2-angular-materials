import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HorasFaltasComponent } from './horas-faltas.component';

describe('HorasFaltasComponent', () => {
  let component: HorasFaltasComponent;
  let fixture: ComponentFixture<HorasFaltasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HorasFaltasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HorasFaltasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
