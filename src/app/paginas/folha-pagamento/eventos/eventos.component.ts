import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EventoFixo } from 'src/app/model/evento-fixo';
import { EventoService } from 'src/app/servicos/evento.service';
import { Evento } from 'src/app/model/evento';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-eventos',
  templateUrl: './eventos.component.html',
  styleUrls: ['./eventos.component.css']
})
export class EventosComponent implements OnInit {
  titulonomemodulo: string = "Folha de Pagamento"
  titulonomepagina: string = "Cadastro de Eventos"

  myForm: FormGroup
  listaEvento: Evento[]
  listaEventoFixo: EventoFixo[]
  alerta: any[] = []

  constructor(
    private _eventoService: EventoService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.load()
    this._eventoService.listar().subscribe(
      (result) => { this.listaEvento = result }
    )
    this.myForm = new FormGroup({
      idevento: new FormControl(),
      incideir: new FormControl(),
      incideinss: new FormControl(),
      incidefgts: new FormControl(),
      tipoevento: new FormControl(),
      descricaoevento: new FormControl(),
    }
    )
  }
  public load() {
    this._eventoService.listar().subscribe(
      (result) => {
        this.listaEvento = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Lançamento Avulso, tente novamente') : ''
      }
    )
  }
  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent)
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }

  public select(evento: Evento): void {
    this.myForm.patchValue(evento);
 }


  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as Evento;

    this._eventoService.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        alert('Cadastrado com sucesso!');
        // this.openDialog()
        // alert('Cadastrado com sucesso!')
      }
    )
    this._eventoService.listar().subscribe(
      (result) => { this.listaEvento = result }
    )
  }
  public delete(evento?: Evento) {
    let retorno = confirm('Deseja Excluir?');
    if (retorno) {
      this._eventoService.delete(evento.idevento).subscribe(
        (resultado) => {
          alert('Deletado com sucesso!');
          this.load();
        }
      );
    }

  }
}
