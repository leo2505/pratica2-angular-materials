import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ListaIrrf } from 'src/app/model/lista-irrf';
import { IrrfService } from 'src/app/servicos/irrf.service';
import { MatDialog } from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';

@Component({
  selector: 'app-tabela-irrf',
  templateUrl: './tabela-irrf.component.html',
  styleUrls: ['./tabela-irrf.component.css']
})
export class TabelaIrrfComponent implements OnInit {
  titulonomemodulo: string = "Folha de Pagamento"
  titulonomepagina: string = "Tabela IRRF"

  myForm: FormGroup
  listairrf: ListaIrrf[]
  alerta: any[] = []
  constructor(
    private _listaIrrf: IrrfService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.load()
    this._listaIrrf.listar().subscribe(
      (result) => { this.listairrf = result }
    )
    this.myForm = new FormGroup({
      idirrf: new FormControl(),
      valorinicial: new FormControl(),
      valorfinal: new FormControl(),
      aliquota: new FormControl(),
      validade: new FormControl(),
      deducaominima: new FormControl(),
      deducaodependente: new FormControl()
    }
    )
  }
  public load() {
    this._listaIrrf.listar().subscribe(
      (result) => {
        this.listairrf = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Lançamento de IRRF, tente novamente') : ''
      }
    )
  }
  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent)
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }

  public select(irrf: ListaIrrf): void {
    this.myForm.patchValue(irrf);
  }

  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as ListaIrrf;

    this._listaIrrf.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        alert('Cadastrado com sucesso!');
        // this.openDialog()
        // alert('Cadastrado com sucesso!')

      }
    )

    this._listaIrrf.listar().subscribe(
      (result) => { this.listairrf = result }
    )
  }
  public delete(irrf?: ListaIrrf) {
    let retorno = confirm('Deseja Excluir?');
    if (retorno) {
      this._listaIrrf.delete(irrf.idirrf).subscribe(
        (resultado) => {
          alert('Deletado com sucesso!');
          this.load();
        }
      );
    }

  }
}
