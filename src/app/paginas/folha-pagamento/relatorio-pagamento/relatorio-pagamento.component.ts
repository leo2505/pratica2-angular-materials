import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';

export interface PeriodoMes {
  valor: string;
  descricao: string;
}

@Component({
  selector: 'app-relatorio-pagamento',
  templateUrl: './relatorio-pagamento.component.html',
  styleUrls: ['./relatorio-pagamento.component.css']
})
export class RelatorioPagamentoComponent implements OnInit {
  titulonomemodulo: string = "Folha Pagamento"
  titulonomepagina: string = "Relatório de Pagamento"

  constructor(
    private _cadastroFuncionarios: CadastroFuncionariosService,
  ) { }

  ngOnInit() {
    this._cadastroFuncionarios.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      periodomes: new FormControl(),
      idfuncionario: new FormControl(),
      ano: new FormControl()
    }
    )
  }
  myForm: FormGroup
  lista: CadastroFuncionarios[]
  listaperiodo: PeriodoMes[] = [
    { valor: '1', descricao: 'Janeiro' },
    { valor: '2', descricao: 'Fevereiro' },
    { valor: '3', descricao: 'Março' },
    { valor: '4', descricao: 'Abril' },
    { valor: '5', descricao: 'Maio' },
    { valor: '6', descricao: 'Junho' },
    { valor: '7', descricao: 'Julho' },
    { valor: '8', descricao: 'Agosto' },
    { valor: '9', descricao: 'Setembro' },
    { valor: '10', descricao: 'Outubro' },
    { valor: '11', descricao: 'Novembro' },
    { valor: '12', descricao: 'Dezembro' },
  ];
  public relatorioFolhaPagamento() {
    var idfuncionario = this.myForm.get('idfuncionario').value;
    var mes = this.myForm.get('periodomes').value;
    var ano = this.myForm.get('ano').value;    
    this._cadastroFuncionarios.relatorioFolhaPagamento(idfuncionario, mes, ano).subscribe(
      (resultado) => {
        const url = window.URL.createObjectURL(resultado);
        window.open(url);
      }
    );
  }
}
