import { Component, OnInit } from '@angular/core';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';

@Component({
  selector: 'app-lista-pessoas',
  templateUrl: './lista-pessoas.component.html',
  styleUrls: ['./lista-pessoas.component.css']
})
export class ListaPessoasComponent implements OnInit {

  constructor(
    private _cadastroCandidato: CadastroCandidatoService
  ) { }

  ngOnInit() {
    this._cadastroCandidato.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  lista: CadastroCandidato[]

}
