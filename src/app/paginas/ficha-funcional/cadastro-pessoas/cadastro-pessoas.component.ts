import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { EstadocivilService } from 'src/app/servicos/estadocivil.service';
import { EstadoCivil } from 'src/app/model/estadocivil';
import { Generos } from 'src/app/model/generos';
import { GenerosService } from 'src/app/servicos/generos.service';
import { CadastroPessoa } from 'src/app/model/cadastro-pessoa';
import { CadastroPessoaService } from 'src/app/servicos/cadastro-pessoa.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
   selector: 'app-cadastro-pessoas',
   templateUrl: './cadastro-pessoas.component.html',
   styleUrls: ['./cadastro-pessoas.component.css']
})
export class CadastroPessoasComponent implements OnInit {
   titulonomemodulo: string = "Ficha Funcional"
   titulonomepagina: string = "Cadastro de Pessoa"
   constructor(
      private _cadastroPessoa: CadastroPessoaService,
      private _estadoCivil: EstadocivilService,
      private _generos: GenerosService
   ) { }

   ngOnInit() {
      this._cadastroPessoa.listar().subscribe(
         (result) => { this.lista = result }
      )
      this.myForm = new FormGroup({
         idpessoa: new FormControl(),
         nomepessoa: new FormControl(),
         datanascimento: new FormControl(),
         contatopessoa: new FormControl(),
         emailpessoa: new FormControl(),
         cpfpessoa: new FormControl(),
         idgenero: new FormControl(),
         idestadocivil: new FormControl()
      }
      )
   }
   myForm: FormGroup
   lista: CadastroPessoa[]
   listaestadocivil: EstadoCivil[]
   listagenero: Generos[]
   alerta: any[] = []
   public load() {
      this._cadastroPessoa.listar().subscribe(
         (result) => {
            this.lista = result
            // console.log(result)
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Pessoas, tente novamente') : ''
         }
      )
      this._estadoCivil.listar().subscribe(
         (result) => {
            this.listaestadocivil = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Estado Civil, tente novamente') : ''
         }
      )
      this._generos.listar().subscribe(
         (result) => {
            this.listagenero = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Gêneros, tente novamente') : ''
         }
      )
   }

   public select(pessoa: CadastroPessoa): void {
      this.myForm.patchValue(pessoa);
   }

   public save(): void {
      const cli =
         Object.assign({}, this.myForm.value) as CadastroPessoa;

      this._cadastroPessoa.save(cli).subscribe(
         (result) => {
            this.load();
         },
         (erro) => {
            alert('erro: ' + erro.message);
         }, () => {
            alert('Cadastrado com sucesso!')

         }
      )
      this._generos.listar().subscribe(
         (result) => {
            this.listagenero = result
         }
      )

      this._cadastroPessoa.listar().subscribe(
         (result) => { this.lista = result }
      )
   }
   public delete(pessoa?: CadastroPessoa) {
      this._cadastroPessoa.delete(pessoa.idpessoa).subscribe(
         (resultado) => {
            alert('Excluído com sucesso!');
            this.load();
         }, () => {

         }
      );
   }

   compareGeneros(x: Generos, y: Generos): boolean {
      return x && y ? x.idgenero === y.idgenero : x === y;
   }

   compareEstadoCivil(x: EstadoCivil, y: EstadoCivil): boolean {
      return x && y ? x.idestadocivil === y.idestadocivil : x === y;
   }
}
