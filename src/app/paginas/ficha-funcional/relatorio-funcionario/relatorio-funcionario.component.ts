import { Component, OnInit } from '@angular/core';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';

@Component({
  selector: 'app-relatorio-funcionario',
  templateUrl: './relatorio-funcionario.component.html',
  styleUrls: ['./relatorio-funcionario.component.css']
})
export class RelatorioFuncionarioComponent implements OnInit {
  titulonomemodulo: string = "Ficha Funcional"
  titulonomepagina: string = "Relatório de Funcionário"
  constructor(
    private _cadastroFuncionarios: CadastroFuncionariosService,
  ) { }

  ngOnInit() {
    this._cadastroFuncionarios.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idfuncionario: new FormControl()
    }
    )
  }

  public relatorioFichaFuncional() {
    var idfuncionario = this.myForm.get('idfuncionario').value;
    this._cadastroFuncionarios.relatorioFichaFuncional(idfuncionario).subscribe(
      (resultado) => {
        const url = window.URL.createObjectURL(resultado);
        window.open(url);
      }
    );
  }
  myForm: FormGroup
  lista: CadastroFuncionarios[]
}
