import { Component, OnInit } from '@angular/core';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatDialog, MatGridList } from '@angular/material';
import { Dependentes } from 'src/app/model/cadastro-dependentes';
import { CadastroDepententesService } from 'src/app/servicos/cadastro-depententes.service';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-dependentes',
  templateUrl: './dependentes.component.html',
  styleUrls: ['./dependentes.component.css']
})
export class DependentesComponent implements OnInit {
  color = 'accent';
  titulonomemodulo: string = "Ficha Funcional"
  titulonomepagina: string = "Dependentes"
  constructor(
    private _cadastroDependentes: CadastroDepententesService,
    private dialog: MatDialog,
    private _cadastroFuncionario: CadastroFuncionariosService
  ) { }

  ngOnInit() {

    this.load()
    this._cadastroDependentes.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      iddependente: new FormControl(),
      idfuncionario: new FormControl(),
      // tipodependente: new FormControl(),
      nomedependente: new FormControl(),
      // rgdependente: new FormControl(),
      cpfdependente: new FormControl(),
      datanascimentodependente: new FormControl(),
      depir: new FormControl(true, [
        Validators.required
      ]),
    }
    )
    
    // console.log(this.myForm)
  }

  openDialog() {
    const dialogRef = this.dialog.open(ModalComponent)
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  myForm: FormGroup
  lista: Dependentes[]
  listafuncionario: CadastroFuncionarios[]
  alerta: any[] = []

  public load() {
    this._cadastroDependentes.listar().subscribe(
      (result) => {
        this.lista = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Dependentes, tente novamente') : ''
      }
    )
    this._cadastroFuncionario.listar().subscribe(
      (result) => {
        this.listafuncionario = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Funcionários, tente novamente') : ''
      }
    )
  }

  public select(dependentes: Dependentes): void {
    this.myForm.patchValue(dependentes);
  }


  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as Dependentes;

    this._cadastroDependentes.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        alert('Cadastrado com suceso!');
        // this.openDialog()
      }
    )

    this._cadastroDependentes.listar().subscribe(
      (result) => { this.lista = result }
    )
    this._cadastroFuncionario.listar().subscribe(
      (result) => { this.listafuncionario = result }
    )
  }
  public delete(funcionario?: Dependentes) {
    let retorno = confirm('Deseja Excluir?');
    this._cadastroDependentes.delete(funcionario.iddependente).subscribe(
      (resultado) => {
        alert('Excluído com sucesso!');
        this.load();
      }, () => {

      }
    );
  }
 

}
