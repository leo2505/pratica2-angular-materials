import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';
import { CadastroFuncionariosService } from 'src/app/servicos/cadastro-funcionarios.service';
import { FormControl, FormGroup } from '@angular/forms';
import { ModalComponent } from 'src/app/componentes/modal/modal.component';
import { Cargos } from 'src/app/model/cargos';
import { CadastroFuncionarios } from 'src/app/model/cadastro-funcionarios';
import { Setores } from 'src/app/model/setores';
import { CargosService } from 'src/app/servicos/cargos.service';
import { SetoresService } from 'src/app/servicos/setores.service';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';
import { JornadaDeTrabalho } from 'src/app/model/jornada-de-trabalho';
import { JornadaDeTrabalhoService } from 'src/app/servicos/jornada-de-trabalho.service';
import { HttpErrorResponse } from '@angular/common/http';
import { ListaPessoasComponent } from '../lista-pessoas/lista-pessoas.component';

@Component({
  selector: 'app-cadastro-funcionarios',
  templateUrl: './cadastro-funcionarios.component.html',
  styleUrls: ['./cadastro-funcionarios.component.css']
})
export class CadastroFuncionariosComponent implements OnInit {
  titulonomemodulo: string = "Ficha Funcional"
  titulonomepagina: string = "Cadastro Funcionários"
  constructor(
    private _cadastroFuncionarios: CadastroFuncionariosService,
    private dialog: MatDialog,
    private _cadastroCandidato: CadastroCandidatoService,
    private _cargos: CargosService,
    private _jornadaDeTrabalho: JornadaDeTrabalhoService,
    private _setores: SetoresService
  ) { }

  ngOnInit() {

    this.load()
    this._cadastroFuncionarios.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idpessoa: new FormControl(),
      idcargo: new FormControl(),
      idsetor: new FormControl(),
      endereco: new FormControl(),
      telefone: new FormControl(),
      numerodependentes: new FormControl(),

      salario: new FormControl(),

      idfuncionario: new FormControl(),
      idhorario: new FormControl(),
      status: new FormControl(),

      nomefuncionario: new FormControl(),
      nomepaifuncionario: new FormControl(),
      nomemaefuncionario: new FormControl(),
      cpffuncionario: new FormControl(),
      rgfuncionario: new FormControl(),
      expedicaorgfuncionario: new FormControl(),
      orgaorgfuncionario: new FormControl(),
      ufrgfuncionario: new FormControl(),

      logradourofuncionario: new FormControl(),
      enderecofuncionario: new FormControl(),
      complementoendereco: new FormControl(),

      emailfuncionario: new FormControl(),
      telefonefuncionario: new FormControl(),

      ctpsfuncionario: new FormControl(),
      ctpsseriefuncionario: new FormControl(),
      ctpsexpedicaofuncionario: new FormControl(),
      pisfuncionario: new FormControl(),

      cnhfuncionario: new FormControl(),
      cnhexpedicaofuncionario: new FormControl(),
      cnhcategoriafuncionario: new FormControl(),
      cnhvencimentofuncionario: new FormControl(),
      cnh1habilitacaofuncionario: new FormControl(),

      salariofuncionario: new FormControl(),
      codbancofuncionario: new FormControl(),
      numcontabancofuncionario: new FormControl(),
      agenciabancofuncionario: new FormControl(),
      dataadmissaofuncionario: new FormControl(),
      contafgtsfuncionario: new FormControl()


    }
    )
    // console.log(this.myForm)
  }
  openDialog() {
    const dialogRef = this.dialog.open(ListaPessoasComponent);
    dialogRef.afterClosed().subscribe(result => {
      // console.log(`Dialog result: ${result}`);
    });
  }
  myForm: FormGroup
  lista: CadastroFuncionarios[]
  listacargos: Cargos[]
  listasetor: Setores[]
  listapessoa: CadastroCandidato[]
  listahorario: JornadaDeTrabalho[]
  horario: JornadaDeTrabalho
  pessoa: string
  setor: Setores
  cargo: string
  alerta: any[] = []
  selecionaHorario() {
    this.horario = this.myForm.get('idhorario').value
  }
  selecionaPessoa() {
    this.pessoa = this.myForm.get('idpessoa').value
  }
  selecionaSetor() {
    this.setor = this.myForm.get('idsetor').value
  }
  selecionaCargo() {
    this.cargo = this.myForm.get('idcargo').value
  }
  public load() {
    this._cadastroFuncionarios.listar().subscribe(
      (result) => {
        this.lista = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Funcionários, tente novamente') : ''
      }
    )
    this._cargos.listar().subscribe(
      (result) => {
        this.listacargos = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Gêneros, tente novamente') : ''
      }
    )
    this._setores.listar().subscribe(
      (result) => {
        this.listasetor = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Setores, tente novamente') : ''
      }
    )
    this._cadastroCandidato.listar().subscribe(
      (result) => {
        this.listapessoa = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Pessoas, tente novamente') : ''
      }
    )
    this._jornadaDeTrabalho.listar().subscribe(
      (result) => {
        this.listahorario = result
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Jornada de Trabalho, tente novamente') : ''
      }
    )
  }
  public save(): void {
    const cli =
      Object.assign({}, this.myForm.value) as CadastroFuncionarios;
    //cli.nomefuncionario =this.myForm.get('idpessoa').value ;
    //cli.ctpsfuncionario =this.myForm.get('ctpsfuncionario').value ;
    this._cadastroFuncionarios.save(cli).subscribe(
      (result) => {
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message)
      }, () => {
        alert('Cadastrado');
        // this.openDialog()
        // alert('Cadastrado com sucesso!')
      }
    )

    this._cadastroFuncionarios.listar().subscribe(
      (result) => { this.lista = result }
    )
    this._cargos.listar().subscribe(
      (result) => { this.listacargos = result }
    )
    this._setores.listar().subscribe(
      (result) => { this.listasetor = result }
    )
    this._cadastroCandidato.listar().subscribe(
      (result) => { this.listapessoa = result }
    )
    this._jornadaDeTrabalho.listar().subscribe(
      (result) => {
        this.listahorario = result
      }
    )
  }
  public delete(funcionario?: CadastroFuncionarios) {
    this._cadastroFuncionarios.delete(funcionario.idfuncionario).subscribe(
      (resultado) => {
        alert('Excluído com sucesso!');
        this.load();
      }, () => {

      }
    );
  }

}
