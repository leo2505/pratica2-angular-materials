import { Component, OnInit } from '@angular/core';
import { CadastroCandidatoService } from 'src/app/servicos/cadastro-candidato.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroCandidato } from 'src/app/model/cadastro-candidato';
import { EstadocivilService } from 'src/app/servicos/estadocivil.service';
import { EstadoCivil } from 'src/app/model/estadocivil';
import { Generos } from 'src/app/model/generos';
import { GenerosService } from 'src/app/servicos/generos.service';
import { HttpErrorResponse } from '@angular/common/http';
import { CidadeService } from 'src/app/servicos/cidade.service';
import { Cidade } from 'src/app/model/cidade-candidato';
import { Escolaridade } from 'src/app/model/escolaridade';
import { EscolaridadeService } from 'src/app/servicos/escolaridade.service';

@Component({
   selector: 'app-cadastro-candidato',
   templateUrl: './cadastro-candidato.component.html',
   styleUrls: ['./cadastro-candidato.component.css']
})

export class CadastroCandidatoComponent implements OnInit {
   titulonomemodulo: string = "Recrutamento e Seleção"
   titulonomepagina: string = "Cadastro de Candidato"
   constructor(private _cadastroCandidato: CadastroCandidatoService,
      private _estadoCivil: EstadocivilService, private _generos: GenerosService,
      private _cidade: CidadeService, private _escolaridade: EscolaridadeService) { }

   ngOnInit() {

      this.load()
      this._cadastroCandidato.listar().subscribe(
         (result) => { this.lista = result }
      )
      this.myForm = new FormGroup({
         idpessoa: new FormControl(),
         nomepessoa: new FormControl(),
         datanascimentopessoa: new FormControl(),
         contatopessoa: new FormControl(),
         emailpessoa: new FormControl(),
         cpfpessoa: new FormControl(),
         idgenero: new FormControl(),
         idcidade: new FormControl(),
         idestadocivil: new FormControl(),
         idescolaridade: new FormControl(),
      }
      )

      // console.log(this.myForm)
   }
   myForm: FormGroup
   lista: CadastroCandidato[]
   listaestadocivil: EstadoCivil[]
   listagenero: Generos[]
   listacidade: Cidade[]
   listaescolaridade: Escolaridade[]
   alerta: any[] = []
   public load() {
      this._cadastroCandidato.listar().subscribe(
         (result) => {
            this.lista = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Pessoas, tente novamente') : ''
         }
      )
      this._estadoCivil.listar().subscribe(
         (result) => {
            this.listaestadocivil = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Estado Civil, tente novamente') : ''
         }
      )
      this._generos.listar().subscribe(
         (result) => {
            this.listagenero = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Gêneros, tente novamente') : ''
         }
      )
      this._cidade.listar().subscribe(
         (result) => {
            this.listacidade = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Cidades, tente novamente') : ''
         }
      )
      this._escolaridade.listar().subscribe(
         (result) => {
            this.listaescolaridade = result
         }, (error: HttpErrorResponse) => {
            (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Escolaridade, tente novamente') : ''
         }
      )
   }

   public select(candidato: CadastroCandidato): void {
      this.myForm.patchValue(candidato);
   }

   public save(): void {
      const cli =
         Object.assign({}, this.myForm.value) as CadastroCandidato;

      this._cadastroCandidato.save(cli).subscribe(
         (result) => {
            this.load();
         },
         (erro) => {
            alert('erro: ' + erro.message);
         }, () => {
            // console.log('Cadastrado');
            alert('Cadastrado com sucesso!')

         }
      )
      /*this._generos.listar().subscribe(
         (result) => {
            this.listagenero = result
            console.log(result)
         }
      )
      this._cidade.listar().subscribe(
         (result) => {
            this.listacidade = result
            console.log(result)
         }
      )-*/

      this._cadastroCandidato.listar().subscribe(
         (result) => { this.lista = result }
      )
   }
   public delete(curriculo?: CadastroCandidato) {
      this._cadastroCandidato.delete(curriculo.idpessoa).subscribe(
         (resultado) => {
            console.log('OK');
            this.load();
         }, () => {

         }
      );
   }

   compareGeneros(x: Generos, y: Generos): boolean {
      return x && y ? x.idgenero === y.idgenero : x === y;
   }

   compareEstadoCivil(x: EstadoCivil, y: EstadoCivil): boolean {
      return x && y ? x.idestadocivil === y.idestadocivil : x === y;
   }
   compareCidade(x: Cidade, y: Cidade): boolean {
      return x && y ? x.idcidade === y.idcidade : x === y;
   }
   compareEscolaridade(x: Escolaridade, y: Escolaridade): boolean {
      return x && y ? x.idescolaridade === y.idescolaridade : x === y;
   }
}
