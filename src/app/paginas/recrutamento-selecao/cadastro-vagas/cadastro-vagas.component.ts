import { Component, OnInit } from '@angular/core';
import { CadastroVagasService } from 'src/app/servicos/cadastro-vagas.service';
import { FormGroup, FormControl } from '@angular/forms';
import { CadastroVagas } from 'src/app/model/cadastro-vagas'
import { Setores } from 'src/app/model/setores'
import { Cargos } from 'src/app/model/cargos'
import { SetoresService } from 'src/app/servicos/setores.service';
import { CargosService } from 'src/app/servicos/cargos.service';
import { TipovagaService } from 'src/app/servicos/tipovaga.service';
import { TipoVaga } from 'src/app/model/tipovaga';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-cadastro-vagas',
  templateUrl: './cadastro-vagas.component.html',
  styleUrls: ['./cadastro-vagas.component.css']
})
export class CadastroVagasComponent implements OnInit {
  titulonomemodulo: string = "Recrutamento e Seleção"
  titulonomepagina: string = "Cadastro de Vagas"
  constructor(
    private _cadastroVagas: CadastroVagasService,
    private _Setores: SetoresService,
    private _Cargos: CargosService,
    private _tipoVaga: TipovagaService
  ) { }

  ngOnInit() {

    this.load()
    this._cadastroVagas.listar().subscribe(
      (result) => { this.lista = result }
    )
    this.myForm = new FormGroup({
      idvaga: new FormControl(),
      idtipovaga: new FormControl(),
      // idescolaridade: new FormControl(),
      descricaovaga: new FormControl(),
      atribuicoesvaga: new FormControl(),
      idsetor: new FormControl(),
      idcargo: new FormControl()
    }
    )
  }
  myForm: FormGroup
  lista: CadastroVagas[]
  listacargos: Cargos[]
  listatipovaga: TipoVaga[]
  listasetores: Setores[]
  alerta: any[] = []

  public load() {
    this._tipoVaga.listar().subscribe(
      (resultvaga) => {
        this.listatipovaga = resultvaga
        // console.log(resultvaga)
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Tipo de Vagas, tente novamente') : ''
      }
    )
    this._cadastroVagas.listar().subscribe(
      (result) => {
        this.lista = result
        // console.log(result)
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Vagas, tente novamente') : ''
      }
    )
    this._Setores.listar().subscribe(
      (resultsetores) => {
        this.listasetores = resultsetores
        // console.log(resultsetores)
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Setores, tente novamente') : ''
      }
    )
    this._Cargos.listar().subscribe(
      (resultcargos) => {
        this.listacargos = resultcargos
        // console.log(resultcargos)
      }, (error: HttpErrorResponse) => {
        (error.status == 0) ? this.alerta.push('Não foi possível conectar a API de Cargos, tente novamente') : ''
      }
    )

  }

  public select(CadastroVagas: CadastroVagas): void {
    this.myForm.patchValue(CadastroVagas);
 }


  public save(): void {
    const cli = Object.assign({}, this.myForm.value) as CadastroVagas;
    this._cadastroVagas.save(cli).subscribe(
      (result) => {
        this.myForm.reset();
        this.load();
      },
      (erro) => {
        alert('erro: ' + erro.message);
      }, () => {
        console.log('Cadastrado');
        alert('Cadastrado com sucesso!')
      }
    )
    this._cadastroVagas.listar().subscribe(
      (result) => { this.lista = result }
    )
  }
  public delete(vaga?: CadastroVagas) {
    let retorno = confirm('Deseja Excluir?');
    this._cadastroVagas.delete(vaga.idvaga).subscribe(
      (resultado) => {
        alert('Excluído com sucesso!');
        this.load();
      }, () => {

      }
    );
  }
}

