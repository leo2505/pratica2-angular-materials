import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelecaoCandidatoComponent } from './selecao-candidato.component';

describe('SelecaoCandidatoComponent', () => {
  let component: SelecaoCandidatoComponent;
  let fixture: ComponentFixture<SelecaoCandidatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelecaoCandidatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelecaoCandidatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
