import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CadastroCandidato } from '../model/cadastro-candidato';

@Injectable({
  providedIn: 'root' // quando é root, disponibiliza p todo o projeto
})
export class CadastroCandidatoService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<CadastroCandidato[]> {
    return this._http
      .get<CadastroCandidato[]>("http://localhost:8080/pessoa")
  }

  public save(candidato: CadastroCandidato): Observable<any> {
      if (candidato.idpessoa > 0) {
         return this._http
         .put("http://localhost:8080/pessoa/" + candidato.idpessoa, candidato)
     
      } else {
         return this._http
         .post("http://localhost:8080/pessoa", candidato)
      }
  }
  public delete(idpessoa: number): Observable<any> {
    return this._http
       .delete("http://localhost:8080/pessoa/"+idpessoa);
 }
}
