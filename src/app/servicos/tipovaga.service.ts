import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { TipoVaga } from '../model/tipovaga';

@Injectable({
  providedIn: 'root'
})
export class TipovagaService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<TipoVaga[]> {
    return this._http
      .get<TipoVaga[]>("http://localhost:8080/tipovaga")
  }
  public save(tipovaga: TipoVaga): Observable<any> {
    return this._http
      .post("http://localhost:8080/tipovaga", tipovaga)
  }
  public delete(tipovaga: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/tipovaga/" + tipovaga);
  }
}
