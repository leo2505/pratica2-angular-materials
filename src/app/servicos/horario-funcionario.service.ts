import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HorarioFuncionario } from '../model/horario-funcionario';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HorarioFuncionarioService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<HorarioFuncionario[]> {
    return this._http
      .get<HorarioFuncionario[]>("http://localhost:8080/horariofuncionario")
  }
  public save(horarioFuncionario: HorarioFuncionario): Observable<any> {

      if (horarioFuncionario.idhorariofuncionario > 0) {
        return this._http
        .put("http://localhost:8080/horariofuncionario/" + horarioFuncionario.idhorariofuncionario, horarioFuncionario)
    
     } else {
        return this._http
        .post("http://localhost:8080/horariofuncionario", horarioFuncionario)
     }

  }
  public delete(idhorariofuncionario: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/horariofuncionario/" + idhorariofuncionario);
  }
  
} 
