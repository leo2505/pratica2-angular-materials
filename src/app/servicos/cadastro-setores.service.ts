import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CadastroSetores } from '../model/cadastro-setores';

@Injectable({
  providedIn: 'root'
})
export class CadastroSetoresService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<CadastroSetores[]> {
    return this._http
      .get<CadastroSetores[]>("http://localhost:8080/setor")
  } 
  public save(setores: CadastroSetores): Observable<any> {
    return this._http
      .post("http://localhost:8080/setor", setores)
      
      if (setores.idsetor > 0) {
        return this._http
        .put("http://localhost:8080/setor/" + setores.idsetor, setores)
    
     } else {
        return this._http
        .post("http://localhost:8080/setor", setores)
     }

    }
  public delete(idsetor: number): Observable<any> {
    return this._http
       .delete("http://localhost:8080/setor/"+idsetor);
 }
}
