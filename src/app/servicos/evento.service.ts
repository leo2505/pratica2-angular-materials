import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Evento } from '../model/evento';

@Injectable({
  providedIn: 'root'
})
export class EventoService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<Evento[]> {
    return this._http
      .get<Evento[]>("http://localhost:8080/evento")
  }
  public save(evento: Evento): Observable<any> {

    if (evento.idevento > 0) {
      return this._http
        .put("http://localhost:8080/evento/" + evento.idevento, evento)

    } else {
      return this._http
        .post("http://localhost:8080/evento", evento)
    }


  }
  public delete(idevento: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/evento/" + idevento);
  }
}
