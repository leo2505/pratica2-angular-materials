import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Setores } from '../model/setores';

@Injectable({
  providedIn: 'root'
})
export class SetoresService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<Setores[]> {
    return this._http
      .get<Setores[]>("http://localhost:8080/setor")
  }
  public save(setores: Setores): Observable<any> {
  
      if (setores.idsetor > 0) {
        return this._http
        .put("http://localhost:8080/setor/" + setores.idsetor, setores)
    
     } else {
        return this._http
        .post("http://localhost:8080/setor", setores)
     }

  
    }
  public delete(idsetor: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/setor/" + idsetor);
  }
}
