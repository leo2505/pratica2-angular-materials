import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JornadaDeTrabalho } from '../model/jornada-de-trabalho';

@Injectable({
  providedIn: 'root'
})
export class JornadaDeTrabalhoService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<JornadaDeTrabalho[]> {
    return this._http
      .get<JornadaDeTrabalho[]>("http://localhost:8080/horario")
  } 
  public save(jornada: JornadaDeTrabalho): Observable<any> {
    return this._http
      .post("http://localhost:8080/horario", jornada)
  
      if (jornada.idhorario > 0) {
        return this._http
        .put("http://localhost:8080/horario/" + jornada.idhorario, jornada)
    
     } else {
        return this._http
        .post("http://localhost:8080/horario", jornada)
     }

    }
  public delete(idhorario: number): Observable<any> {
    return this._http
       .delete("http://localhost:8080/horario/"+idhorario);
 }
}
