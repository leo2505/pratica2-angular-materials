import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LancamentoAvulso } from '../model/lancamento-avulso';

@Injectable({
  providedIn: 'root'
})
export class LancamentoAvulsoService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<LancamentoAvulso[]> {
    return this._http
      .get<LancamentoAvulso[]>("http://localhost:8080/lancavulso")
  }
  public save(lancamentoAvulso: LancamentoAvulso): Observable<any> {

    if (lancamentoAvulso.idlancamento > 0) {
      return this._http
        .put("http://localhost:8080/lancavulso/" + lancamentoAvulso.idlancamento, lancamentoAvulso)

    } else {
      return this._http
        .post("http://localhost:8080/lancavulso", lancamentoAvulso)
    }

  }
  public delete(idlancamento: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/lancavulso/" + idlancamento);
  }
}
