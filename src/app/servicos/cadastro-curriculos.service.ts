import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CadastroCurriculos } from '../model/cadastro-curriculos';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadastroCurriculosService {

  constructor(private _http: HttpClient) { }
  public listar(): Observable<CadastroCurriculos[]> {
    return this._http
      .get<CadastroCurriculos[]>("http://localhost:8080/curriculo")
  }

  public save(curriculos: CadastroCurriculos): Observable<any> {
    return this._http
      .post("http://localhost:8080/curriculo", curriculos)
  }
  public delete(idcurriculo: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/curriculo/" + idcurriculo);
  }
}
