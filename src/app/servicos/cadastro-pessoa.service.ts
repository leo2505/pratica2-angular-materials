import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CadastroPessoa } from '../model/cadastro-pessoa';

@Injectable({
  providedIn: 'root'
})
export class CadastroPessoaService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<CadastroPessoa[]> {
    return this._http
      .get<CadastroPessoa[]>("http://localhost:8080/pessoa")
  }

  public save(pessoa: CadastroPessoa): Observable<any> {
      if (pessoa.idpessoa > 0) {
         return this._http
         .put("http://localhost:8080/pessoa/" + pessoa.idpessoa, pessoa)
      
      } else {
         return this._http
         .post("http://localhost:8080/pessoa", pessoa)
      }
  }
  public delete(idpessoa: number): Observable<any> {
    return this._http
       .delete("http://localhost:8080/pessoa/"+idpessoa);
 }
}
