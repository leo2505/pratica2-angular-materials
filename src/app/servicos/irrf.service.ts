import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ListaIrrf } from '../model/lista-irrf';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IrrfService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<ListaIrrf[]> {
    return this._http
      .get<ListaIrrf[]>("http://localhost:8080/irrf")
  }
  public save(listairrf: ListaIrrf): Observable<any> {

  
      if (listairrf.idirrf > 0) {
        return this._http
        .put("http://localhost:8080/irrf/" + listairrf.idirrf, listairrf)
    
     } else {
        return this._http
        .post("http://localhost:8080/irrf", listairrf)
     }

    }
  public delete(idirrf: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/irrf/" + idirrf);
  }
}
