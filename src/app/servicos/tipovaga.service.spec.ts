import { TestBed } from '@angular/core/testing';

import { TipovagaService } from './tipovaga.service';

describe('TipovagaService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TipovagaService = TestBed.get(TipovagaService);
    expect(service).toBeTruthy();
  });
});
