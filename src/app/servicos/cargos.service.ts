import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cargos } from '../model/cargos';

@Injectable({
  providedIn: 'root'
})
export class CargosService {

  constructor(private _http: HttpClient) { }

  
  public listar(): Observable<Cargos[]> {
    return this._http
      .get<Cargos[]>("http://localhost:8080/cargo")
  } 
  public save(cargos: Cargos): Observable<any> {

      if (cargos.idcargo > 0) {
        return this._http
        .put("http://localhost:8080/cargo/" + cargos.idcargo, cargos)
    
     } else {
        return this._http
        .post("http://localhost:8080/cargo", cargos)
     }

  }
  public delete(idcargo: number): Observable<any> {
    return this._http
       .delete("http://localhost:8080/cargo/"+idcargo);
 }
}
