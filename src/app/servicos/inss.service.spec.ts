import { TestBed } from '@angular/core/testing';

import { InssService } from './inss.service';

describe('InssService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InssService = TestBed.get(InssService);
    expect(service).toBeTruthy();
  });
});
