import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EstadoCivil } from '../model/estadocivil';

@Injectable({
  providedIn: 'root'
})
export class EstadocivilService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<EstadoCivil[]> {
    return this._http
      .get<EstadoCivil[]>("http://localhost:8080/estadocivil")
  } 
  public save(estadocivil: EstadoCivil): Observable<any> {
    return this._http
      .post("http://localhost:8080/estadocivil", estadocivil)
  }
}
 