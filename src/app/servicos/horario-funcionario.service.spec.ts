import { TestBed } from '@angular/core/testing';

import { HorarioFuncionarioService } from './horario-funcionario.service';

describe('HorarioFuncionarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: HorarioFuncionarioService = TestBed.get(HorarioFuncionarioService);
    expect(service).toBeTruthy();
  });
});
