import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Dependentes } from '../model/cadastro-dependentes';

@Injectable({
  providedIn: 'root'
})
export class CadastroDepententesService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<Dependentes[]> {
    return this._http
      .get<Dependentes[]>("http://localhost:8080/dependente")
  } 
  public save(dependentes: Dependentes): Observable<any> {

      if (dependentes.iddependente > 0) {
        return this._http
        .put("http://localhost:8080/dependente/" + dependentes.iddependente, dependentes)
    
     } else {
        return this._http
        .post("http://localhost:8080/dependente", dependentes)
     }


  }
  public delete(iddependente: number): Observable<any> {
    return this._http
       .delete("http://localhost:8080/dependente/"+iddependente);
 }
}
