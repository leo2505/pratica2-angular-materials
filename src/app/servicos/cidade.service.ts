import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Cidade } from '../model/cidade-candidato';

@Injectable({
  providedIn: 'root' // quando é root, disponibiliza p todo o projeto
})
export class CidadeService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<Cidade[]> {
    return this._http
      .get<Cidade[]>("http://localhost:8080/cidade")
  }
}
