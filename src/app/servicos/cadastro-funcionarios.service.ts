import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CadastroFuncionarios } from '../model/cadastro-funcionarios';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadastroFuncionariosService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<CadastroFuncionarios[]> {
    return this._http
      .get<CadastroFuncionarios[]>("http://localhost:8080/funcionario")
  }
  public save(cadastrofuncionarios: CadastroFuncionarios): Observable<any> {
    return this._http
      .post("http://localhost:8080/funcionario", cadastrofuncionarios)
  }
  public delete(idfuncionario: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/funcionario/" + idfuncionario);
  }
  public relatorioFichaFuncional(idfuncionario: string): Observable<any> {
    return this._http
      .get("http://localhost:8080/funcionario/report/?idfuncionario=" + idfuncionario, { responseType: "blob" });

  }
  public relatorioFolhaPagamento(idfuncionario: number, mes: number, ano: number): Observable<any> {
    return this._http
      .get("http://localhost:8080/calculo/salarioliquido/?idfuncionario=" + idfuncionario + "&mes=" + mes + "&ano=" + ano, { responseType: "blob" });

  }
}
