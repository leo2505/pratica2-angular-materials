import { TestBed } from '@angular/core/testing';

import { LancamentoAvulsoService } from './lancamento-avulso.service';

describe('LancamentoAvulsoService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LancamentoAvulsoService = TestBed.get(LancamentoAvulsoService);
    expect(service).toBeTruthy();
  });
});
