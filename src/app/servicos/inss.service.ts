import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ListaInss } from '../model/lista-inss';

@Injectable({
  providedIn: 'root'
})
export class InssService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<ListaInss[]> {
    return this._http
      .get<ListaInss[]>("http://localhost:8080/inss")
  }
  public save(listainss: ListaInss): Observable<any> {
    //

    if (listainss.idinss > 0) {
      return this._http
        .put("http://localhost:8080/inss/" + listainss.idinss, listainss)

    } else {
      return this._http
        .post("http://localhost:8080/inss", listainss)
    }

  }
  public delete(idinss: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/inss/" + idinss);
  }
}
