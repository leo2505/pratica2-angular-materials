import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Escolaridade } from '../model/escolaridade';

@Injectable({
  providedIn: 'root' // quando é root, disponibiliza p todo o projeto
})
export class EscolaridadeService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<Escolaridade[]> {
    return this._http
      .get<Escolaridade[]>("http://localhost:8080/escolaridade")
  }
}
