import { TestBed } from '@angular/core/testing';

import { IrrfService } from './irrf.service';

describe('IrrfService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IrrfService = TestBed.get(IrrfService);
    expect(service).toBeTruthy();
  });
});
