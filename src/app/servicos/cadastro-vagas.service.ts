import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CadastroVagas } from '../model/cadastro-vagas';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CadastroVagasService {

  constructor(private _http: HttpClient) { }

  public listar(): Observable<CadastroVagas[]> {
    return this._http
      .get<CadastroVagas[]>("http://localhost:8080/vaga")
  }
  public save(cli: CadastroVagas): Observable<any> {


    if (cli.idvaga > 0) {
      return this._http
        .put("http://localhost:8080/vaga/" + cli.idvaga, cli)

    } else {
      return this._http
        .post("http://localhost:8080/vaga", cli)
    }


  }
  public delete(idvaga: number): Observable<any> {
    return this._http
      .delete("http://localhost:8080/vaga/" + idvaga);
  }
}
