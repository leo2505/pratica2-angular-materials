import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavLateralComponent } from './nav-lateral/nav-lateral.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatCheckboxModule, MatSelectModule, MatRippleModule, MatInputModule, MatFormFieldModule, MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatGridListModule, MatCardModule, MatMenuModule, MatTableModule, MatPaginatorModule, MatSortModule, MatDialogModule, MAT_DIALOG_DEFAULT_OPTIONS, MatProgressBarModule, MatStepperModule, MatSlideToggle, MatSlideToggleModule, MatDatepicker, MatDatepickerModule, MatNativeDateModule, MAT_DATE_LOCALE, MatProgressSpinnerModule } from '@angular/material';
import { DashboardPrincipalComponent } from './dashboard-principal/dashboard-principal.component';
import { TabelaComponent } from './tabela/tabela.component';
import { RecrutamentoSelecaoComponent } from './paginas/recrutamento-selecao/recrutamento-selecao.component';
import { HomeComponent } from './paginas/home/home.component';
import { CadastroVagasComponent } from './paginas/recrutamento-selecao/cadastro-vagas/cadastro-vagas.component';
import { CadastroCandidatoComponent } from './paginas/recrutamento-selecao/cadastro-candidato/cadastro-candidato.component';
import { SelecaoCandidatoComponent } from './paginas/recrutamento-selecao/selecao-candidato/selecao-candidato.component';
import { MatFileUploadModule } from 'angular-material-fileupload';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CadastroCandidatoService } from './servicos/cadastro-candidato.service';
import { CadastroFuncionariosComponent } from './paginas/ficha-funcional/cadastro-funcionarios/cadastro-funcionarios.component';
import { ListaFuncionariosComponent } from './paginas/ficha-funcional/lista-funcionarios/lista-funcionarios.component';
import { HorasFaltasComponent } from './paginas/folha-pagamento/horas-faltas/horas-faltas.component';

import { TabelaDescontosComponent } from './paginas/folha-pagamento/tabela-descontos/tabela-descontos.component';
import { DependentesComponent } from './paginas/ficha-funcional/dependentes/dependentes.component';
import { CadastroCurriculosComponent } from './paginas/recrutamento-selecao/cadastro-curriculo/cadastro-curriculos.component';
import { CargosComponent } from './paginas/configuracoes/cargos/cargos.component';
import { SetorComponent } from './paginas/configuracoes/setor/setor.component';
import { ModalComponent } from './componentes/modal/modal.component';
import { SucessoComponent } from './componentes/sucesso/sucesso.component';
import { JornadaDeTrabalhoComponent } from './paginas/configuracoes/jornada-de-trabalho/jornada-de-trabalho.component';
import { Grafico1Component } from './componentes/grafico1/grafico1.component';
import { CadastroPessoasComponent } from './paginas/ficha-funcional/cadastro-pessoas/cadastro-pessoas.component';
import { HorarioFuncionarioComponent } from './paginas/folha-pagamento/horario-funcionario/horario-funcionario.component';
import { TabelaInssComponent } from './paginas/folha-pagamento/tabela-inss/tabela-inss.component';
import { TabelaIrrfComponent } from './paginas/folha-pagamento/tabela-irrf/tabela-irrf.component';
import { LancamentoAvulsoComponent } from './paginas/folha-pagamento/lancamento-avulso/lancamento-avulso.component';
import { EventosComponent } from './paginas/folha-pagamento/eventos/eventos.component';
import { RelatorioPagamentoComponent } from './paginas/folha-pagamento/relatorio-pagamento/relatorio-pagamento.component';
import { RelatorioFuncionarioComponent } from './paginas/ficha-funcional/relatorio-funcionario/relatorio-funcionario.component';
import { ListaPessoasComponent } from './paginas/ficha-funcional/lista-pessoas/lista-pessoas.component';



@NgModule({
  declarations: [
    AppComponent,
    NavLateralComponent,
    DashboardPrincipalComponent,
    TabelaComponent,
    RecrutamentoSelecaoComponent,
    HomeComponent,
    CadastroVagasComponent,
    CadastroCandidatoComponent,
    SelecaoCandidatoComponent,
    CadastroFuncionariosComponent,
    ListaFuncionariosComponent,
    HorasFaltasComponent,
    TabelaDescontosComponent,
    DependentesComponent,
    CadastroCurriculosComponent,
    CargosComponent,
    SetorComponent,
    ModalComponent,
    SucessoComponent,
    JornadaDeTrabalhoComponent,
    Grafico1Component,
    CadastroPessoasComponent,
    HorarioFuncionarioComponent,
    TabelaInssComponent,
    TabelaIrrfComponent,
    LancamentoAvulsoComponent,
    EventosComponent,
    RelatorioPagamentoComponent,
    RelatorioFuncionarioComponent,
    ListaPessoasComponent
  ],
  exports: [
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatSelectModule,
    ListaPessoasComponent
  ],
  imports: [
    MatCheckboxModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatFileUploadModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatStepperModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatSlideToggleModule,
    MatDatepickerModule,
    MatNativeDateModule,

  ],
  providers: [CadastroCandidatoService, { provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: { hasBackdrop: false } }, { provide: MAT_DATE_LOCALE, useValue: 'pt-BR' }],
  entryComponents: [ModalComponent, ListaPessoasComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
